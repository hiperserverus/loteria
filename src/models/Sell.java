
package models;




import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;
import views.Principal;

/**
 *
 * @author Machine
 */
public class Sell {
public static DefaultTableModel modelo = new DefaultTableModel();
private Principal principalView = new Principal();
public String[] listTimes =  {"09:00 AM","10:00 AM", "11:00 AM", "12:00 PM", "01:00 PM","03:00 PM", "04:00 PM", "05:00 PM", "06:00 PM", "07:00 PM"};
public String[] listAnimals = {"0", "1", "2", "3", "4", "5", "6",
                             "7", "8", "9", "10", "11", "12", "13",
                             "14","15", "16", "17", "18", "19", "20",
                             "21","22", "23", "24", "25", "26", "27",
                             "28","29", "30", "31", "32", "33", "34",
                             "35","36", "37"};


   public void  loadMethods() {
        showTable();
        addTimes();
        addAnimals();
 }

 void showTable() {
      modelo.addColumn("Sorteo");
      modelo.addColumn("Jugada");
      modelo.addColumn("Monto");
      principalView.tblTicket.setModel(modelo);
 }

 void addTimes() {

     JLabel times[] = {principalView.lblTime1,principalView.lblTime2,principalView.lblTime3,principalView.lblTime4,
                        principalView.lblTime5,principalView.lblTime6,principalView.lblTime7,principalView.lblTime8,principalView.lblTime9,principalView.lblTime10};

       for(int i = 0; i < this.listTimes.length; i++) {

            times[i].setText(this.listTimes[i]);
   }

 }

  void addAnimals(){

JLabel animals[] = {principalView.lbl1,principalView.lbl2,principalView.lbl3,principalView.lbl4,principalView.lbl5,principalView.lbl6,principalView.lbl7
                    ,principalView.lbl8,principalView.lbl9,principalView.lbl10,principalView.lbl11,principalView.lbl12,principalView.lbl13,principalView.lbl14
                    ,principalView.lbl15,principalView.lbl16,principalView.lbl17,principalView.lbl18,principalView.lbl19,principalView.lbl20,principalView.lbl21
                    ,principalView.lbl22,principalView.lbl23,principalView.lbl24,principalView.lbl25,principalView.lbl26,principalView.lbl27,principalView.lbl28
                    ,principalView.lbl29,principalView.lbl30,principalView.lbl31,principalView.lbl32,principalView.lbl33,principalView.lbl34,principalView.lbl35
                    ,principalView.lbl36,principalView.lbl37,principalView.lbl38};

   for(int i = 0; i < this.listAnimals.length; i++) {

     animals[i].setIcon(new ImageIcon(getClass().getResource("../images/"+ this.listAnimals[i]+".gif")));

   }
 
 }


 public void addPlay(String[] data) {
        modelo.addRow(data);
 }








}
